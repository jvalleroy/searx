# /usr/bin/env python3
#
# Copyright: 2017 Johannes Schauer <josch@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

import sys
import json

args = json.load(sys.stdin)

print("""
@font-face {
	font-family:"%s";""" % args["fontFamilyName"])

srcs = []
for t in args["types"]:
    if t == "woff":
        srcs.append("""url("../fonts/ion.woff") format("woff")""")
    elif t == "woff2":
        srcs.append("""url("../fonts/ion.woff2") format("woff2")""")
    elif t == "ttf":
        srcs.append("""url("../fonts/ion.ttf") format("truetype")""")
    elif t == "svg":
        srcs.append("""url("../fonts/ion.svg") format("svg")""")
    else:
        raise Exception("unknown font type: %s" % t)

print("\tsrc:%s;" % ",\n\t\t".join(srcs))

print("""
	font-weight:normal;
	font-style:normal;
}

.ion-icon {
	&:before {
		font-family:"%s";
	}
	display:inline-block;
	vertical-align:middle;
	line-height:1;
	font-weight:normal;
	font-style:normal;
	speak:none;
	text-decoration:inherit;
	text-transform:none;
	text-rendering:auto;
	-webkit-font-smoothing:antialiased;
	-moz-osx-font-smoothing:grayscale;
}
""" % args["fontFamilyName"])

for k in sorted(args["codepoints"].keys()):
    print("""
.%s-%s {
	&:before {
		content:"\\%s";
	}
}""" % (args["fontFamilyName"], k, hex(args["codepoints"][k])[2:]))
